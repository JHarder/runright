package com.runright;

import com.runright.Sprite;

import org.newdawn.slick.Input;
import org.newdawn.slick.GameContainer;

public class Player extends Sprite {
    static final int UP    = 0;
    static final int LEFT  = 1;
    static final int DOWN  = 2;
    static final int RIGHT = 3;
    
    private Sprite sprite;
    private boolean[] directions;
    private float speed;
    private int jumpHeight;
    private boolean canJump;

    private static float gravityMax = 20f;
    private float dy;

    public Player(String image, float x, float y) {
        super(image,x,y);
        directions = new boolean[4];
        speed = 5;
        jumpHeight = 0;
        canJump = true;
        dy = 0;
    }

    public void update(GameContainer gc, int i) {
        directions[UP] =    gc.getInput().isKeyDown(Input.KEY_UP);
        directions[LEFT] =  gc.getInput().isKeyDown(Input.KEY_LEFT);
        directions[DOWN] =  gc.getInput().isKeyDown(Input.KEY_DOWN);
        directions[RIGHT] = gc.getInput().isKeyDown(Input.KEY_RIGHT);

        if(directions[LEFT])  move(-speed,0);
        if(directions[RIGHT]) move(speed,0);
        if(directions[UP] && canJump) {
            dy = -10;
            move(0, dy);
            canJump = false;
            return;
        }

        // if(directions[RIGHT]) move(speed,0);
        if(y >= 450) {
            y = 450;
            dy = 0;
            canJump = true;
        }
        if(dy < gravityMax && y < 450) {
            dy++;
        }
        move(0,dy);
    }

    // public void attack
}
