package com.runright;

import com.runright.Player;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Input;
import org.newdawn.slick.tiled.TiledMap;

public class SlickTest extends BasicGame {
    private static AppGameContainer app;
    private TiledMap map;
    private Player player;
    private int x;

    public SlickTest(String gamename) {
        super(gamename);
    }

    @Override
    public void init(GameContainer gc) throws SlickException {
        map = new TiledMap("resources/maps/test3.tmx");
        player = new Player("resources/dirt.png", 50, 50);
        System.out.println(map.getTileId(10,10,0));
        x = 100;
    }

    @Override
    public void update(GameContainer gc, int i) throws SlickException {
        boolean hasClosed = gc.getInput().isKeyDown(Input.KEY_ESCAPE);
        if(hasClosed) {
            app.exit();
        }
        player.update(gc,i);
        // x -= 3;
    }

    @Override
    public void render(GameContainer gc, Graphics g) throws SlickException {
        g.clear();
        map.render(x,0);
        player.draw();

    }
    
    public static void main(String[] args) {
        try {
            app = new AppGameContainer(new SlickTest("Simple slick game"));
            app.setDisplayMode(640, 480, false);
            app.start();
        } catch(SlickException ex) {
            System.out.println("you dun goofed");
        }
    }

}
