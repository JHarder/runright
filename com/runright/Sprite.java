package com.runright;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Sprite {
    private Image img;
    protected float x, y;

    public Sprite(String image, float x, float y) {
        try {
            img = new Image(image);
            this.x = x;
            this.y = y;
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
    public void move(float x_dif, float y_dif) {
        x+=x_dif;
        y+=y_dif;
    }
    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
    }
    public void draw() {
        img.draw(x,y);
    }

    public int getTileX() {
        // [       |      |       ]
        //       tileMap-Width
        //   Screen-------Width 
        //
        // offset = (screenwidth-tileMapWidth) / 2
        // return (x-offset)/32;
        return 0;
    }
    public int getTileY() {
        return 0;
    }
}
